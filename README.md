Speed Vise Handle for Machinists' Vise
======================================

Threads use [threadlib](https://github.com/adrianschlatter/threadlib); follow the installation
instructions there.

The parameters above the threadlib invocation are meant to be changed to fit.  Note in particular
the nominal measurement of the socket size for the vise; not all vises use the same size socket.

The parts are oriented to print without supports.  I used 9
perimeters, 40% cubic infill, and 0.3mm layers in PLA.

The screw hole in the end of the support is for a countersunk
screw; I used M8x60. Set `screw_length` to 1mm or so longer than the
screw you actually have. Note that the length of a countersunk
screw includes the head.

Copyright © Michael K Johnson
[Licensed under the Creative Commons - Attribution license.](http://creativecommons.org/licenses/by/3.0/)

