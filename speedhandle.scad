// speed vise handle for machinists' vise
// Copyright © Michael K Johnson
// Licensed under the Creative Commons - Attribution license.
// http://creativecommons.org/licenses/by/3.0/

// Set parameters above threadlib inclusion as desired
// Parts are oriented to print without supports
// Print with plenty of perimeters and substantial infill

socket_size= 5/8 * 25.4; // 5/8" — 0.625 * 25.4 = 15.875
base=60; // lengths between centers of holes in crank (sockets and handle)
margin=6; // thickness of crank outside sockets and handle nut
depth=10; // depth of crank
standoff_depth = 2; // depth of additional height to move handle out from vise
chamfer=1.5; // height of crank chamfer
twelve_center=true; // false for six-point center socket, true for twelve-point socket
handle_length=70; // total including depth of crank
screw_length=61; // total length for handle screw (countersink screw, including head) including gap
handle_margin=0.5; // space between bolt and handle
handle_thick=5; // thickness of shell of handle and of bolt head
thread="M8"; // thread spec

use <threadlib/threadlib.scad> // https://github.com/adrianschlatter/threadlib

socket_d = socket_size / sin(60);
specs = thread_specs(str(thread, "-int"));
major_d = specs[2];
pitch = specs[0];

shoulder_d = major_d + 6;
handle_outer_d = handle_thick*2+handle_margin*2+shoulder_d;

module socket(twelve=false, depth=depth) {
    cylinder(d=socket_d, h=depth+1, center=true, $fn=6);
    if (twelve) {
        rotate([0, 0, 30]) cylinder(d=socket_d, h=depth+1, center=true, $fn=6);
    }
}
module crank() {
    // centered at 1/2 depth
    z=depth-2*chamfer;
    wd=2*chamfer;
    difference() {
        union() {
            minkowski() {
                union() {
                    hull() {
                        // handle bolt hole
                        translate([base, 0, 0]) cylinder(d=handle_outer_d-wd, h=z, center=true, $fn=120);
                        // center socket (not including standoff)
                        cylinder(d=socket_d + margin*2 - wd, h=z, center=true);
                        // end socket
                        translate([-base, 0, 0]) cylinder(d=socket_d + margin*2 - wd, h=z, center=true, $fn=120);
                    }
                }
                union() {
                    cylinder(h=chamfer, r1=chamfer, r2=0);
                    rotate([180, 0, 0]) cylinder(h=chamfer, r1=chamfer, r2=0);
                }
            }
            // center socket standoff
            translate([0, 0, z/2+standoff_depth/2+chamfer/2]) cylinder(d=socket_d + margin * 2, h=standoff_depth+chamfer, center=true, $fn=120);
        }
        // space for bolt for handle
        translate([base, 0, 0]) cylinder(d=major_d+1, h=depth+1, center=true, $fn=120);
        translate([base, 0, depth/2-major_d/2]) cylinder(h=major_d, d1=0, d2=major_d*2, center=true, $fn=120);
        // central socket including standoff
        translate([0, 0, depth/2-standoff_depth]) socket(twelve=twelve_center, depth=(depth+standoff_depth)*5);
        // end socket
        translate([-base, 0, 0]) socket();
    }
}

module shoulder_spacer() {
    nut_offset = handle_length-screw_length;
    difference() {
        // spacer body
        union() {
            // body
            translate([0, 0, nut_offset + pitch/2]) nut(thread, Douter=shoulder_d, turns=((screw_length-depth)/pitch)-1);
            // filler
            cylinder(d=shoulder_d, h=nut_offset, $fn=120);
            // head
            cylinder(d=shoulder_d + handle_thick, h=handle_thick, $fn=120);
        }
        // screwdriver slot
        translate([0, 0, handle_thick/4])
            cube([shoulder_d, 1.5, handle_thick/2], center=true);
    }
}
module handle() {
    difference() {
        cylinder(d=handle_outer_d, h=handle_length-depth, $fn=120);
        cylinder(d=handle_margin*2+shoulder_d, h=handle_length-depth, $fn=120);
        translate([0, 0, handle_length - depth - handle_thick - handle_margin])
            cylinder(d=shoulder_d + handle_thick + handle_margin*2, h=handle_thick+2*handle_margin, $fn=120);
    }
}
module print_set() {
    translate([0, 0, depth/2]) crank();
    translate([-socket_d*2, socket_d*2, 0]) shoulder_spacer();
    translate([socket_d*2, socket_d*2, 0]) handle();
}
module view_set() {
    translate([0, 0, depth/2]) rotate([180, 0, 0]) crank();
    translate([base, 0, handle_length]) rotate([180, 0, 0]) shoulder_spacer();
    translate([base, 0, depth]) handle();
}
print_set();
//view_set();